import Vue from 'vue'
import App from './App'
import router from './router'
import log from './plugins/log'

Vue.config.productionTip = false
Vue.use(log)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})

import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/Index'
import Debugger from '@/components/Debugger'
import ConsoleLogger from '@/components/ConsoleLogger'
import AsyncRequest from '@/components/AsyncRequest'
const LazyComponent = () => import('@/components/LazyComponent')

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Index',
            component: Index,
        },
        {
            path: '/async-request',
            component: AsyncRequest,
        },
        {
            path: '/debugger',
            component: Debugger,
        },
        {
            path: '/i-am-lazy',
            component: LazyComponent,
        },
        {
            path: '/consoleum',
            component: ConsoleLogger,
        },

    ]
})
